import { createSlice, Draft, PayloadAction } from '@reduxjs/toolkit'
import { FieldType, generateAgents, generateCodeCard } from './utils/game'
import { RootState } from './app/store'
import { random } from './utils/number'
import { GameState, Orientation, Side, Team } from './Game.types'

import DefaultGame from './assets/gametypes/default.json'
import DefaultEnGame from './assets/gametypes/default_en.json'
import DuettGame from './assets/gametypes/duett.json'
import UndercoverGame from './assets/gametypes/undercover.json'
import { isTouchDevice } from './utils/detect'

const GameTypes = {
  default: DefaultGame,
  default_en: DefaultEnGame,
  undercover: UndercoverGame,
  duett: DuettGame,
}

const initialState: GameState = {
  codecard: [],
  agents: [],
  config: {
    gameType: 'default',
    gridSize: 5,
    bombs: 1,
    agents: 8,
    orientation: isTouchDevice() ? Orientation.Auto : Orientation.Down,
    startTeam: Team.Red,
    sfxEnabled: true,
    musicEnabled: false,
  },
  state: {
    redAgentsCount: 0,
    blueAgentsCount: 0,
    activeTeam: undefined,
    secretServiceChief: Side.Right,
    comboCounter: 0,
    exposedAgents: [],
    exposedRedAgents: [],
    exposedBlueAgents: [],
    winner: undefined,
    winCounter: {
      red: 0,
      blue: 0,
    },
  },
}

const _newCodeCard = (state: Draft<GameState>) => {
  state.codecard = generateCodeCard(state.config)
}

const _checkGameEnd = (state: Draft<GameState>) => {
  if (
    state.state.exposedBlueAgents.length >= state.state.blueAgentsCount ||
    state.state.exposedRedAgents.length >= state.state.redAgentsCount
  ) {
    const winnerTeam =
      state.state.exposedBlueAgents.length >= state.state.blueAgentsCount ? Team.Blue : Team.Red
    _endGame(state, { payload: winnerTeam, type: '' })
  }
}

const _endGame = (state: Draft<GameState>, action: PayloadAction<Team>) => {
  state.state.winner = action.payload
  if (action.payload === Team.Red) {
    state.state.winCounter.red++
  } else {
    state.state.winCounter.blue++
  }
}

const _exposeAgent = (state: Draft<GameState>, action: PayloadAction<string>) => {
  if (state.state.exposedAgents.indexOf(action.payload) > -1) return
  state.state.exposedAgents.push(action.payload)
  const agentIndex = state.agents.indexOf(action.payload)
  const field = state.codecard[agentIndex]
  switch (field) {
    case FieldType.Red:
      state.state.exposedRedAgents.push(action.payload)
      if (state.state.activeTeam === Team.Blue) {
        _endRound(state)
      } else {
        state.state.comboCounter++
      }
      _checkGameEnd(state)
      break
    case FieldType.Blue:
      state.state.exposedBlueAgents.push(action.payload)
      if (state.state.activeTeam === Team.Red) {
        _endRound(state)
      } else {
        state.state.comboCounter++
      }
      _checkGameEnd(state)
      break
    case FieldType.Bomb:
      if (state.state.activeTeam === Team.Red) {
        _endGame(state, { payload: Team.Blue, type: '' })
      } else {
        _endGame(state, { payload: Team.Red, type: '' })
      }
      break
    default:
      _endRound(state)
      _checkGameEnd(state)
  }
}

const _endRound = (state: Draft<GameState>) => {
  state.state.activeTeam = state.state.activeTeam === Team.Red ? Team.Blue : Team.Red
  state.state.comboCounter = 0

  _checkGameEnd(state)
}

export const gameSlice = createSlice({
  name: 'game',
  initialState,
  reducers: {
    newCodeCard: _newCodeCard,
    setBombs: (state, action: PayloadAction<number>) => {
      state.config.bombs = action.payload
    },
    setGridSize: (state, action: PayloadAction<number>) => {
      if (action.payload > 3) {
        state.config.gridSize = action.payload
      }
    },
    setOrientation: (state, action: PayloadAction<Orientation>) => {
      state.config.orientation = action.payload
    },
    setAgents: (state, action: PayloadAction<number>) => {
      if (action.payload > 1) {
        state.config.agents = action.payload
      }
    },
    setGameType: (state, action: PayloadAction<string>) => {
      state.config.gameType = action.payload
    },
    setSfxEnabled: (state, action: PayloadAction<boolean>) => {
      state.config.sfxEnabled = action.payload
    },
    exposeAgent: _exposeAgent,
    endRound: _endRound,
    startNewGame: (state) => {
      state.state.secretServiceChief =
        state.state.secretServiceChief === Side.Left ? Side.Right : Side.Left
      state.state.activeTeam = state.config.startTeam = random(0, 1)
      state.state.blueAgentsCount =
        state.config.agents + (state.config.startTeam === Team.Blue ? 1 : 0)
      state.state.redAgentsCount =
        state.config.agents + (state.config.startTeam === Team.Red ? 1 : 0)
      state.codecard = generateCodeCard(state.config)
      // @ts-ignore
      state.agents = generateAgents(state.config.gridSize, GameTypes[state.config.gameType])
      state.state.winner = undefined
      state.state.comboCounter = 0
      state.state.exposedAgents = []
      state.state.exposedRedAgents = []
      state.state.exposedBlueAgents = []
    },
  },
})

// Action creators are generated for each case reducer function
export const {
  newCodeCard,
  setGameType,
  setBombs,
  setGridSize,
  setOrientation,
  setAgents,
  setSfxEnabled,
  exposeAgent,
  endRound,
  startNewGame,
} = gameSlice.actions
export const codeCardState = gameSlice.getInitialState()

export const selectCodeCard = (state: RootState) => state.game.codecard
export const selectPlayground = (state: RootState) => state.game.config
export const selectAgents = (state: RootState) => state.game.agents
export const selectExposedAgents = (state: RootState) => state.game.state.exposedAgents
export const selectExposedRedAgents = (state: RootState) => state.game.state.exposedRedAgents
export const selectExposedBlueAgents = (state: RootState) => state.game.state.exposedBlueAgents
export const selectActiveTeam = (state: RootState) => state.game.state.activeTeam
export const selectWinCounter = (state: RootState) => state.game.state.winCounter
export const selectComboCounter = (state: RootState) => state.game.state.comboCounter
export const selectWinner = (state: RootState) => state.game.state.winner
export const selectSecretServiceChief = (state: RootState) => state.game.state.secretServiceChief
export const selectAgentType = (state: RootState) => (agent: string) =>
  state.game.codecard[state.game.agents.indexOf(agent)]

export default gameSlice.reducer

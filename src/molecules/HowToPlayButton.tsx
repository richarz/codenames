import React from 'react'
import { Button } from '@mui/material'
import { useTranslation } from 'react-i18next'

const HowToPlayButton = () => {
  const { t } = useTranslation()

  return (
    <Button
      variant="contained"
      href="https://www.ultraboardgames.com/codenames/game-rules.php"
      target="_blank"
      color="secondary"
      rel="nofollow"
    >
      {t('common.rulesButton')}
    </Button>
  )
}

export default HowToPlayButton

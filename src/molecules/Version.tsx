import React from 'react'
import GhostText from '../atoms/GhostText'

const Version = () => {
  return <GhostText variant="caption">Version: {process.env.REACT_APP_VERSION}</GhostText>
}

export default Version

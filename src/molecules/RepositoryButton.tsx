import React from 'react'
import { Button } from '@mui/material'
import { useTranslation } from 'react-i18next'

const RepositoryButton = () => {
  const { t } = useTranslation()

  return (
    <Button
      variant="contained"
      href="https://gitlab.com/richarz/codenames"
      target="_blank"
      rel="nofollow"
    >
      {t('common.repositoryButton')}
    </Button>
  )
}

export default RepositoryButton

import Pop from '../assets/sfx/pop.mp3'
import { HookOptions } from 'use-sound/dist/types'
import useSoundControlled from '../utils/useSound'

const usePop = (options?: HookOptions) => useSoundControlled(Pop, options)

export default usePop

import DunDunDun from '../assets/sfx/dun-dun-dun.mp3'
import { HookOptions } from 'use-sound/dist/types'
import useSoundControlled from '../utils/useSound'

const useDunDunDun = (options?: HookOptions) => useSoundControlled(DunDunDun, options)

export default useDunDunDun

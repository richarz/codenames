import EnableSound from '../assets/sfx/enable-sound.mp3'
import { HookOptions } from 'use-sound/dist/types'
import useSoundControlled from '../utils/useSound'

const useEnableSound = (options?: HookOptions) => useSoundControlled(EnableSound, options)

export default useEnableSound

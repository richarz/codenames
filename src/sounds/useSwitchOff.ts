import SwitchOff from '../assets/sfx/switch-off.mp3'
import { HookOptions } from 'use-sound/dist/types'
import useSoundControlled from '../utils/useSound'

const useSwitchOff = (options?: HookOptions) => useSoundControlled(SwitchOff, options)

export default useSwitchOff

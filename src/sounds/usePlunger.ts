import Plunger from '../assets/sfx/plunger.mp3'
import { HookOptions } from 'use-sound/dist/types'
import useSoundControlled from '../utils/useSound'

const usePlunger = (options?: HookOptions) => useSoundControlled(Plunger, options)

export default usePlunger

import PopDown from '../assets/sfx/pop-down.mp3'
import { HookOptions } from 'use-sound/dist/types'
import useSoundControlled from '../utils/useSound'

const usePopDown = (options?: HookOptions) => useSoundControlled(PopDown, options)

export default usePopDown

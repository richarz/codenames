import GlugA from '../assets/sfx/glug-a.mp3'
import { HookOptions } from 'use-sound/dist/types'
import useSoundControlled from '../utils/useSound'

const useGlugA = (options?: HookOptions) => useSoundControlled(GlugA, options)

export default useGlugA

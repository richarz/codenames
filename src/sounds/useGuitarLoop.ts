import GuitarLoop from '../assets/sfx/guitar-loop.mp3'
import { HookOptions } from 'use-sound/dist/types'
import useSoundControlled from '../utils/useSound'

const useGuitarLoop = (options?: HookOptions) => useSoundControlled(GuitarLoop, options)

export default useGuitarLoop

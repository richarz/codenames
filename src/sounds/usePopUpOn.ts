import PopUpOn from '../assets/sfx/pop-up-on.mp3'
import { HookOptions } from 'use-sound/dist/types'
import useSoundControlled from '../utils/useSound'

const usePopUpOn = (options?: HookOptions) => useSoundControlled(PopUpOn, options)

export default usePopUpOn

import Bite from '../assets/sfx/bite.mp3'
import { HookOptions } from 'use-sound/dist/types'
import useSoundControlled from '../utils/useSound'

const useBite = (options?: HookOptions) => useSoundControlled(Bite, options)

export default useBite

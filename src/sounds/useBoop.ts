import Boop from '../assets/sfx/boop.mp3'
import { HookOptions } from 'use-sound/dist/types'
import useSoundControlled from '../utils/useSound'

const useBoop = (options?: HookOptions) => useSoundControlled(Boop, options)

export default useBoop

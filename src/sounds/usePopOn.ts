import PopOn from '../assets/sfx/pop-on.mp3'
import { HookOptions } from 'use-sound/dist/types'
import useSoundControlled from '../utils/useSound'

const usePopOn = (options?: HookOptions) => useSoundControlled(PopOn, options)

export default usePopOn

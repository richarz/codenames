import Drums from '../assets/sfx/909-drums.mp3'
import { HookOptions } from 'use-sound/dist/types'
import useSoundControlled from '../utils/useSound'

const useDrums = (options?: HookOptions) => useSoundControlled(Drums, options)

export default useDrums

import Meow from '../assets/sfx/meow.mp3'
import { HookOptions } from 'use-sound/dist/types'
import useSoundControlled from '../utils/useSound'

const useMeow = (options?: HookOptions) => useSoundControlled(Meow, options)

export default useMeow

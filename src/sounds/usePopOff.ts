import PopOff from '../assets/sfx/pop-off.mp3'
import { HookOptions } from 'use-sound/dist/types'
import useSoundControlled from '../utils/useSound'

const usePopOff = (options?: HookOptions) => useSoundControlled(PopOff, options)

export default usePopOff

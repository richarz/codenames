import MenuOpen from '../assets/sfx/menu-open.mp3'
import { HookOptions } from 'use-sound/dist/types'
import useSoundControlled from '../utils/useSound'

const useMenuOpen = (options?: HookOptions) => useSoundControlled(MenuOpen, options)

export default useMenuOpen

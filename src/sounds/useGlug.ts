import Glug from '../assets/sfx/glug.mp3'
import { HookOptions } from 'use-sound/dist/types'
import useSoundControlled from '../utils/useSound'

const useGlug = (options?: HookOptions) => useSoundControlled(Glug, options)

export default useGlug

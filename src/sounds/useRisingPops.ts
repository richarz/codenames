import RisingPops from '../assets/sfx/rising-pops.mp3'
import { HookOptions } from 'use-sound/dist/types'
import useSoundControlled from '../utils/useSound'

const useRisingPops = (options?: HookOptions) => useSoundControlled(RisingPops, options)

export default useRisingPops

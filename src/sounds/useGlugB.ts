import GlugB from '../assets/sfx/glug-b.mp3'
import { HookOptions } from 'use-sound/dist/types'
import useSoundControlled from '../utils/useSound'

const useGlugB = (options?: HookOptions) => useSoundControlled(GlugB, options)

export default useGlugB

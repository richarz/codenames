import Fanfare from '../assets/sfx/fanfare.mp3'
import { HookOptions } from 'use-sound/dist/types'
import useSoundControlled from '../utils/useSound'

const useFanfare = (options?: HookOptions) => useSoundControlled(Fanfare, options)

export default useFanfare

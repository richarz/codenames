import DisableSound from '../assets/sfx/disable-sound.mp3'
import { HookOptions } from 'use-sound/dist/types'
import useSoundControlled from '../utils/useSound'

const useDisableSound = (options?: HookOptions) => useSoundControlled(DisableSound, options)

export default useDisableSound

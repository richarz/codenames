import PopUpOff from '../assets/sfx/pop-up-off.mp3'
import { HookOptions } from 'use-sound/dist/types'
import useSoundControlled from '../utils/useSound'

const usePopUpOff = (options?: HookOptions) => useSoundControlled(PopUpOff, options)

export default usePopUpOff

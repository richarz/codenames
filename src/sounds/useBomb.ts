import Bomb from '../assets/sfx/bomb.mp3'
import { HookOptions } from 'use-sound/dist/types'
import useSoundControlled from '../utils/useSound'

const useBomb = (options?: HookOptions) => useSoundControlled(Bomb, options)

export default useBomb

import PlungerImmediate from '../assets/sfx/plunger-immediate.mp3'
import { HookOptions } from 'use-sound/dist/types'
import useSoundControlled from '../utils/useSound'

const usePlungerImmediate = (options?: HookOptions) => useSoundControlled(PlungerImmediate, options)

export default usePlungerImmediate

import SwitchOn from '../assets/sfx/switch-on.mp3'
import { HookOptions } from 'use-sound/dist/types'
import useSoundControlled from '../utils/useSound'

const useSwitchOn = (options?: HookOptions) => useSoundControlled(SwitchOn, options)

export default useSwitchOn

import Pfff from '../assets/sfx/pfff.mp3'
import { HookOptions } from 'use-sound/dist/types'
import useSoundControlled from '../utils/useSound'

const usePfff = (options?: HookOptions) => useSoundControlled(Pfff, options)

export default usePfff

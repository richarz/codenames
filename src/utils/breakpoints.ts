import { useTheme } from '@mui/material/styles'
import { useMediaQuery } from '@mui/material'

export const useBreakpoint = () => {
  const theme = useTheme()
  return {
    isTabletOrBigger: useMediaQuery(theme.breakpoints.up('md')),
    isDesktopOrBigger: useMediaQuery(theme.breakpoints.up('lg')),
  }
}

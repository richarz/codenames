import useSound from 'use-sound'
import { useAppSelector } from '../app/hooks'
import { selectPlayground } from '../Game.reducer'
import { HookOptions } from 'use-sound/dist/types'

const useSoundControlled = (src: string | string[], options?: HookOptions) =>
  useSound(src, {
    soundEnabled: useAppSelector(selectPlayground).sfxEnabled,
    ...options,
  })

export default useSoundControlled

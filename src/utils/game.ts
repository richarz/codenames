import { random } from './number'
import { shuffle } from './array'
import { Orientation, Team } from '../Game.types'

export enum FieldType {
  PasserBy = 0,
  Red = 1,
  Blue = 2,
  Bomb = 3,
}

export interface CodeCardOptions {
  gameType: string
  gridSize: number
  orientation: Orientation
  agents: number
  bombs: number
  startTeam: Team
  sfxEnabled: boolean
  musicEnabled: boolean
}

export const generateCodeCard = ({
  gridSize = 5,
  agents = 9,
  bombs = 1,
  startTeam = 0,
}: CodeCardOptions): number[] => {
  const counts = {
    agentsRed: 0,
    agentsBlue: 0,
    bombs: 0,
  }

  const size = gridSize * gridSize

  const cells = Array(size).fill(FieldType.PasserBy)

  const redAgentsCount = agents + (startTeam === Team.Red ? 1 : 0)
  const blueAgentsCount = agents + (startTeam === Team.Blue ? 1 : 0)
  const hasEmptyCells = (arr: FieldType[]) => arr.some((cell) => cell === FieldType.PasserBy)

  // Place Bombs
  while (counts.bombs < bombs) {
    if (!hasEmptyCells(cells)) {
      break
    }
    const index = random(0, size - 1)
    if (cells[index] === FieldType.PasserBy) {
      cells[index] = FieldType.Bomb
      counts.bombs++
    }
  }
  // Place Red Agents
  while (counts.agentsRed < redAgentsCount) {
    if (!hasEmptyCells(cells)) {
      break
    }
    const index = random(0, size - 1)
    if (cells[index] === FieldType.PasserBy) {
      cells[index] = FieldType.Red
      counts.agentsRed++
    }
  }
  // Place Blue Agents
  while (counts.agentsBlue < blueAgentsCount) {
    if (!hasEmptyCells(cells)) {
      break
    }
    const index = random(0, size - 1)
    if (cells[index] === FieldType.PasserBy) {
      cells[index] = FieldType.Blue
      counts.agentsBlue++
    }
  }

  return cells
}

export const generateAgents = (gridSize: number = 5, words: string[]) =>
  shuffle(words).slice(0, gridSize * gridSize)

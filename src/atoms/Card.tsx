import { styled } from '@mui/material/styles'
import { Paper } from '@mui/material'

export default styled(Paper)(({ theme }) => ({
  width: '100%',
  aspectRatio: '14/9',
  display: 'inline-block',
  background: 'linear-gradient(135deg, #fdfcfb 0%, #e2d1c3 100%)',
  border: '1px solid rgba(0,0,0,.85)',
  borderRadius: '1.25em',
  color: 'black',
  textAlign: 'center',
  fontWeight: 'bold',
  fontSize: '.4rem',
  [theme.breakpoints.up('sm')]: {
    fontSize: '.5rem',
  },
  [theme.breakpoints.up('md')]: {
    fontSize: '.75rem',
  },
  [theme.breakpoints.up('lg')]: {
    fontSize: '.9rem',
  },
  [theme.breakpoints.up('xl')]: {
    fontSize: '1.3rem',
  },
  userSelect: 'none',
  cursor: 'pointer',
  textTransform: 'uppercase',
}))

import { styled } from '@mui/material/styles'
import { Typography } from '@mui/material'

export default styled(Typography)(({ theme }) => ({
  color: 'rgba(255,255,255,.35)',
}))

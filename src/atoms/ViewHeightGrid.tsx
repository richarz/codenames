import { styled } from '@mui/material/styles'
import { Grid } from '@mui/material'

export default styled(Grid)(({ theme }) => ({
  height: '100vh',
}))

import { styled } from '@mui/material/styles'
import { Button } from '@mui/material'

export default styled(Button)(({ theme }) => ({
  paddingTop: '2rem !important',
  paddingBottom: '2rem !important',
  fontWeight: 'bold !important',
  fontSize: '.5rem',
  [theme.breakpoints.up('sm')]: {
    fontSize: '.6rem',
    paddingTop: '2.5rem !important',
    paddingBottom: '2.5rem !important',
  },
  [theme.breakpoints.up('md')]: {
    fontSize: '.8rem',
    paddingTop: '3rem !important',
    paddingBottom: '3rem !important',
  },
}))

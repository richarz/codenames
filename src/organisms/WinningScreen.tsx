import React from 'react'
import { Box, Grid, Typography } from '@mui/material'
import { Team } from '../Game.types'
import { selectWinCounter, selectWinner, startNewGame } from '../Game.reducer'
import BigButton from '../atoms/BigButton'
import ViewHeightGrid from '../atoms/ViewHeightGrid'
import { useAppDispatch, useAppSelector } from '../app/hooks'
import { makeStyles } from '@mui/styles'
import classNames from 'classnames'
import { useTranslation } from 'react-i18next'

interface WinningScreenProps {
  show: boolean
}

const useStyle = makeStyles(() => ({
  winning: {
    zIndex: 10000,
    position: 'absolute',
    visibility: 'hidden',
    opacity: 0,
    top: 0,
    left: 0,
    background: 'rgba(0,0,0,0.7)',
    transition: 'visibility 300ms, opacity 300ms',
  },
  show: {
    visibility: 'visible',
    opacity: 1,
  },
  center: {
    textAlign: 'center',
  },
  points: {
    fontSize: '15rem !important',
  },
  bold: {
    fontWeight: 'bolder !important'
  },
  redText: {
    color: 'rgba(229,26,43,1)',
  },
  Red: {
    background: 'rgba(59,22,25,.8)',
  },
  blueText: {
    color: 'rgba(9,128,162,1)',
  },
  Blue: {
    background: 'rgba(16,50,60,.8)',
  },
}))

const WinningScreen: React.FC<WinningScreenProps> = ({ show }) => {
  const { t } = useTranslation()
  const classes = useStyle()
  const winner = useAppSelector(selectWinner)
  const { red, blue } = useAppSelector(selectWinCounter)
  const dispatch = useAppDispatch()
  const handleNewGamePress = () => dispatch(startNewGame())

  return (
    <ViewHeightGrid
      container={true}
      justifyContent="center"
      alignItems="center"
      className={classNames(classes.winning, {
        [classes.show]: show,
        [classes.Red]: winner === Team.Red,
        [classes.Blue]: winner === Team.Blue,
      })}
    >
      <Grid item={true} xs={3} justifyContent={'center'} className={classes.center}>
        <Typography variant={'h1'} className={classNames(classes.points, classes.redText, {
          [classes.bold]: winner === Team.Red
        })}>
          {red}
        </Typography>
      </Grid>
      <Grid item={true} container={true} xs={6} justifyContent={'center'}>
        <Box p={4}>
          <Typography variant={'h1'} className={winner === Team.Red ? classes.redText : classes.blueText}>
            {t('winning.teamWon', {
              team: winner === Team.Red ? t('winning.red') : t('winning.blue'),
            })}
          </Typography>
        </Box>
        <BigButton
          fullWidth={true}
          variant="contained"
          color="primary"
          onClick={handleNewGamePress}
        >
          {t('winning.nextGame')}
        </BigButton>
      </Grid>
      <Grid item={true} xs={3} justifyContent={'center'} className={classes.center}>
        <Typography variant={'h1'} className={classNames(classes.points, classes.blueText)}>
          {blue}
        </Typography>
      </Grid>
    </ViewHeightGrid>
  )
}

export default WinningScreen

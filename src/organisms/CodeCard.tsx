import React, { useState } from 'react'
import {Box, Button, Grid, Typography} from '@mui/material'
import { makeStyles } from '@mui/styles'
import classNames from 'classnames'
import { FieldType } from '../utils/game'
import { useAppSelector } from '../app/hooks'
import { selectPlayground } from '../Game.reducer'
import {useTranslation} from "react-i18next"

export interface CodeCardProps {
  codeCard: number[]
  overlay?: boolean
}

const useStyle = makeStyles((theme) => ({
  container: {
    height: '100svmin !important',
    width: '100svmin !important',
    display: 'flex',
    flexDirection: 'column',
    userSelect: 'none',
  },
  codecard: {
    flex: 1,
    position: 'relative',
  },
  overlay: {
    color: 'black',
    textAlign: 'center',
    padding: '15%',
    position: 'absolute',
    background:
        'radial-gradient(circle, rgba(130,130,130,1) 0%, rgba(110,110,110,1) 41%, rgba(80,80,80,1) 100%)',
    height: '100%',
    width: '100%',
    opacity: 1,
    transition: 'opacity 250ms',
  },
  hide: {
    opacity: 0,
  },
  ColorPatch: {
    borderRadius: '3%',
    border: '4px solid black',
    paddingBottom: '1vw !important',
  },
  PasserBy: {
    background:
        'radial-gradient(circle, rgba(213,195,155,1) 0%, rgba(167,152,120,1) 41%, rgba(83,76,61,1) 100%)',
  },
  Red: {
    background:
        'radial-gradient(circle, rgba(228,78,90,1) 0%, rgba(229,26,43,1) 41%, rgba(59,22,25,1) 100%)',
  },
  Blue: {
    background:
        'radial-gradient(circle, rgba(50,180,217,1) 0%, rgba(9,128,162,1) 41%, rgba(16,50,60,1) 100%)',
  },
  Bomb: {
    background:
        'radial-gradient(circle, rgba(85,83,80,1) 0%, rgba(60,58,56,1) 41%, rgba(40,39,38,1) 100%)',
  },
}))

const CodeCard: React.FC<CodeCardProps> = ({ codeCard, overlay = true }) => {
  const classes = useStyle()
  const { t } = useTranslation()
  const { gridSize } = useAppSelector(selectPlayground)

  const [pressed, setPressed] = useState(false)

  const style = {
    width: `${100 / gridSize - 1}%`,
    paddingBottom: `${100 / gridSize - 1}%`,
  }

  const handleMouseDown = (event: React.MouseEvent<HTMLElement>) => {
    setPressed(true)
  }
  const handleMouseUp = (event: React.MouseEvent<HTMLElement>) => {
    setPressed(false)
  }
  const handleMouseOut = (event: React.MouseEvent<HTMLElement>) => {
    setPressed(false)
  }
  const handleTouchStart = (event: React.TouchEvent<HTMLElement>) => {
    setPressed(true)
  }
  const handleTouchEnd = (event: React.TouchEvent<HTMLElement>) => {
    setPressed(false)
  }

  return (
    <div
        className={classes.container}
        onMouseDown={handleMouseDown}
        onMouseUp={handleMouseUp}
        onMouseOut={handleMouseOut}
        onTouchStart={handleTouchStart}
        onTouchEnd={handleTouchEnd}
    >
      <Button sx={{mb:2}} fullWidth disabled>{t('codecard.agent')}</Button>
      <Grid
        container={true}
        spacing={2}
        justifyContent={'space-between'}
        className={classNames(classes.codecard)}
      >
        {codeCard.map((cell, index) => (
          <Grid
            style={style}
            key={`cell-${index}`}
            className={classNames(classes.ColorPatch, {
              // @ts-ignore
              [classes[FieldType[cell]]]: true,
            })}
          />
        ))}
        {overlay && (
          <Box
            className={classNames(classes.overlay, {
              [classes.hide]: pressed,
            })}
          >
            <Typography variant="h3" sx={{textTransform: 'uppercase', fontFamily: 'fantasy, Arial, monospace'}} color="error">{t('codecard.topSecret')}</Typography>
            <Typography variant="h5">{t('codecard.keepPressedToDisplay')}</Typography>
          </Box>
        )}
      </Grid>
      <Button sx={{mb: 3}} fullWidth disabled>{t('codecard.spymaster')}</Button>
    </div>
  )
}

export default CodeCard

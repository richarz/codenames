import React from 'react'
import { useTranslation } from 'react-i18next'
import classNames from 'classnames'
import { Side, Team } from '../Game.types'
import { Box, Grid, Typography } from '@mui/material'
import QRCodeCard from './QRCodeCard'
import BigButton from '../atoms/BigButton'
import { useBreakpoint } from '../utils/breakpoints'
import usePlungerImmediate from '../sounds/usePlungerImmediate'
import { endRound } from '../Game.reducer'
import { useAppDispatch } from '../app/hooks'
import { makeStyles } from '@mui/styles'

const useStyle = makeStyles(() => ({
  mirrored: {
    transform: 'scale(-1, -1)',
  },
  redTeam: {
    transition: 'transform 250ms',
    background: 'rgba(229,26,43,.3)',
  },
  redActive: {
    background: 'rgba(229,26,43,.7)',
  },
  blueTeam: {
    transition: 'transform 250ms',
    background: 'rgba(9,120,162,.35)',
  },
  blueActive: {
    background: 'rgba(9,120,162,.8)',
  },
  points: {
    color: 'rgba(255,255,255,.75)',
  },
  rotateLeft: {
    transform: 'rotate(90deg)',
  },
  rotateRight: {
    transform: 'rotate(-90deg)',
  },
}))

export interface TeamControlProps {
  side: Side
  team: Team
  active: boolean
  mirrored: boolean
  exposedAgents: string[]
}

const TeamControl: React.FC<TeamControlProps> = ({
  side,
  team,
  active,
  mirrored,
  exposedAgents,
}) => {
  const classes = useStyle()
  const { t } = useTranslation()
  const { isTabletOrBigger } = useBreakpoint()
  const dispatch = useAppDispatch()

  const [endRoundSound] = usePlungerImmediate()

  const handleEndRound = () => {
    dispatch(endRound())
    endRoundSound()
  }

  return (
    <Grid
      item={true}
      container={true}
      className={classNames({
        [classes.redTeam]: team === Team.Red,
        [classes.blueTeam]: team === Team.Blue,
        [classes.redActive]: team === Team.Red && active,
        [classes.blueActive]: team === Team.Blue && active,
        [classes.mirrored]: mirrored,
      })}
      xs={9}
      md={7}
      direction={'column-reverse'}
      alignItems="center"
      justifyContent={'space-between'}
    >
      <Box pb={4}>
        <QRCodeCard />
      </Box>
      <Typography
        variant={isTabletOrBigger ? 'h1' : 'h2'}
        className={classNames(classes.points, {
          [classes.rotateRight]: mirrored,
          [classes.rotateLeft]: !mirrored,
        })}
      >
        {exposedAgents.length}
      </Typography>
      <BigButton
        disabled={!active}
        className={classes.mirrored}
        variant="contained"
        color="success"
        size="large"
        fullWidth={true}
        onClick={handleEndRound}
      >
        {t('game.endRound')}
      </BigButton>
    </Grid>
  )
}

export default TeamControl

import React from 'react'
import { Box, Button, Dialog, DialogTitle, Grid, Typography } from '@mui/material'
import Impressum from '../molecules/Impressum'
import { useTranslation } from 'react-i18next'
import HowToPlayButton from '../molecules/HowToPlayButton'
import RepositoryButton from '../molecules/RepositoryButton'

interface AboutDialogProps {
  onClose: () => any
  open: boolean
}

const AboutDialog: React.FC<AboutDialogProps> = ({ onClose, open }) => {
  const { t } = useTranslation()

  const handleClose = () => {
    onClose()
  }

  return (
    <Dialog onClose={handleClose} open={open} fullWidth={true} maxWidth="md">
      <DialogTitle>{t('about.title')}</DialogTitle>
      <Box p={4}>
        <Grid container={true} justifyContent={'space-between'}>
          <Typography>{t('about.developedBy')}</Typography>
          <HowToPlayButton />
          <RepositoryButton />
        </Grid>
      </Box>
      <Box p={4}>
        <Impressum />
      </Box>
      <Box p={4}>
        <Button variant="contained" color="secondary" size="large" onClick={handleClose}>
          {t('common.close')}
        </Button>
      </Box>
    </Dialog>
  )
}

export default AboutDialog

import React from 'react'
import { makeStyles } from '@mui/styles'
import { Theme } from '@mui/material'
import { useAppSelector } from '../app/hooks'
import { selectAgentType, selectCodeCard, selectExposedAgents } from '../Game.reducer'
import classNames from 'classnames'
import { FieldType } from '../utils/game'
import AgentSvg from '../assets/agent.svg'
import BombSvg from '../assets/bomb.svg'
import Card from '../atoms/Card'
import { useTheme } from '@mui/material/styles'

interface WordCardProps {
  agent: string
  index: number
  onClick?: (agent: string, type: FieldType) => void
}

const useStyle = makeStyles((theme: Theme) => ({
  card: {
    position: 'relative',
  },
  mirrored: {
    fontSize: '1em',
    textAlign: 'start',
    color: 'rgba(109, 80, 0, 0.55)',
    transform: 'scale(-1, -1)',
    borderTop: '1px solid rgba(109, 80, 0, 0.35)',
    paddingBottom: '2vh',
    paddingLeft: '.5em',
    [theme.breakpoints.up('sm')]: {
      borderTop: '3px solid rgba(109, 80, 0, 0.35)',
      paddingLeft: '.75em',
    },
    [theme.breakpoints.up('md')]: {
      borderTop: '5px solid rgba(109, 80, 0, 0.35)',
      paddingLeft: '1em',
    },
    textTransform: 'uppercase',
  },
  title: {
    marginTop: '.25em',
    fontSize: '1em',
    textTransform: 'uppercase',
    padding: '.25em',
    background: 'linear-gradient(120deg, #fdfbfb 0%, #ebedee 100%)',
  },
  overlay: {
    visibility: 'hidden',
    opacity: 0,
    position: 'absolute',
    top: -57,
    left: -57,
    width: 'calc(200% + 14px)',
    height: 'calc(200% + 14px)',
    borderRadius: 7,
    border: '3px solid black',
    transition:
      'height 300ms, width 300ms, opacity 300ms, top 300ms, left 300ms, transform 600ms, z-index 600ms',
    transform: 'rotate(0deg)',
    boxShadow:
      '0px 0px 7px -4px rgba(0,0,0,0.4),0px 0px 15px 1px rgba(0,0,0,0.24),0px 0px 20px 3px rgba(0,0,0,0.22)',
    zIndex: 10000,
    overflow: 'hidden',
  },
  exposed: {
    zIndex: 100,
    visibility: 'visible',
    opacity: 1,
    top: -7,
    left: -7,
    width: 'calc(100% + 14px)',
    height: 'calc(100% + 14px)',
    // increase box shadow
  },
  PasserBy: {
    background:
      'radial-gradient(circle, rgba(213,195,155,1) 0%, rgba(167,152,120,1) 41%, rgba(83,76,61,1) 100%)',
    transform: 'rotate(.5deg)',
  },
  Red: {
    background:
      'radial-gradient(circle, rgba(228,78,90,1) 0%, rgba(229,26,43,1) 41%, rgba(59,22,25,1) 100%)',
    transform: 'rotate(-1.5deg)',
  },
  Blue: {
    background:
      'radial-gradient(circle, rgba(50,180,217,1) 0%, rgba(9,128,162,1) 41%, rgba(16,50,60,1) 100%)',
    transform: 'rotate(1.5deg)',
  },
  Bomb: {
    background:
      'radial-gradient(circle, rgba(85,83,80,1) 0%, rgba(60,58,56,1) 41%, rgba(40,39,38,1) 100%)',
    transform: 'rotate(-.25deg)',
  },
}))

const AgentCard: React.FC<WordCardProps> = ({ agent, index, onClick }) => {
  const isExposed = useAppSelector(selectExposedAgents).indexOf(agent) > -1
  const team = useAppSelector(selectCodeCard)[index]
  const type = useAppSelector(selectAgentType)(agent)
  const theme = useTheme()
  const classes = useStyle(theme)
  const handleAgentCardPress = () => onClick && onClick(agent, type)
  return (
    <Card elevation={4} className={classNames(classes.card)} onClick={handleAgentCardPress}>
      <div className={classes.mirrored}>{agent}</div>
      <div className={classes.title}>{agent}</div>
      <div
        className={classNames(classes.overlay, {
          [classes.exposed]: isExposed,
          [classes.Red]: isExposed && team === FieldType.Red,
          [classes.Blue]: team === FieldType.Blue,
          [classes.PasserBy]: team === FieldType.PasserBy,
          [classes.Bomb]: team === FieldType.Bomb,
        })}
      >
          {team === FieldType.Red && (
              <img src={AgentSvg}
                  style={{ width: '50%', opacity: 0.25, marginTop: '7%'}}
                  alt="Agent Red"
              />
          )}
          {team === FieldType.Blue && (
              <img src={AgentSvg}
                   style={{width: '50%', opacity: 0.25, marginTop: '7%'}}
                   alt="Agent Blue"
              />
          )}
          {team === FieldType.Bomb && (
              <img src={BombSvg}
                   style={{width: '50%', opacity: 0.35, marginTop: '7%' }}
                   alt="Bomb"
              />
          )}
      </div>
    </Card>
  )
}

export default AgentCard

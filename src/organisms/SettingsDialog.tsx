import React, { MouseEvent } from 'react'
import {
  Box,
  Button,
  Dialog,
  DialogTitle,
  FormControl,
  IconButton,
  InputLabel,
  MenuItem,
  Select,
  SelectChangeEvent,
  Slider,
  ToggleButton,
  ToggleButtonGroup,
  Typography,
} from '@mui/material'
import { useAppDispatch, useAppSelector } from '../app/hooks'
import {
  selectPlayground,
  setAgents,
  setBombs,
  setGameType,
  setGridSize,
  setOrientation,
  setSfxEnabled,
} from '../Game.reducer'
import VolumeUpIcon from '@mui/icons-material/VolumeUp'
import VolumeOffIcon from '@mui/icons-material/VolumeOff'
import AutorenewIcon from '@mui/icons-material/Autorenew'
import ArrowUpwardIcon from '@mui/icons-material/ArrowUpward'
import ArrowDownwardIcon from '@mui/icons-material/ArrowDownward'
import { Orientation } from '../Game.types'
import { useTranslation } from 'react-i18next'
import NewGameButton from './NewGameButton'

interface SettingsDialogProps {
  onClose: () => any
  open: boolean
}

const SettingsDialog: React.FC<SettingsDialogProps> = ({ onClose, open }) => {
  const { t, i18n } = useTranslation()
  const { gridSize, bombs, agents, gameType, sfxEnabled, orientation } =
    useAppSelector(selectPlayground)
  const dispatch = useAppDispatch()
  const handleClose = () => {
    onClose()
  }
  const handleLanguageChange = (event: React.MouseEvent<HTMLElement>, newLanguage: string) => {
    i18n.changeLanguage(newLanguage)
  }
  const handleChangeSfxEnabled = () => dispatch(setSfxEnabled(!sfxEnabled))
  const handleChangeGameType = (event: SelectChangeEvent) =>
    dispatch(setGameType(event.target.value as string))
  const handleChangeGridSize = (event: Event, value: number | number[]) =>
    dispatch(setGridSize(value as number))
  const handleChangeBombs = (event: Event, value: number | number[]) =>
    dispatch(setBombs(value as number))
  const handleChangeAgents = (event: Event, value: number | number[]) =>
    dispatch(setAgents(value as number))
  const handleChangeOrientation = (event: MouseEvent<HTMLElement>, value: string) => {
    if (value !== undefined && value !== null) {
      dispatch(setOrientation(parseInt(value, 10)))
    }
  }

  return (
    <Dialog onClose={handleClose} open={open} fullWidth={true} maxWidth="md">
      <DialogTitle>{t('settings.title')}</DialogTitle>
      <Box p={4}>
        <Box p={1}>
          <Typography variant={'h6'}>{t('settings.language.title')}</Typography>
          <ToggleButtonGroup
            value={i18n.language}
            exclusive={true}
            onChange={handleLanguageChange}
            aria-label={t('settings.language.label')}
          >
            <ToggleButton
              value="de-DE"
              aria-label={t('settings.language.de')}
              title={t('settings.language.de')}
            >
              DE
            </ToggleButton>
            <ToggleButton
              value="en-EN"
              aria-label={t('settings.language.en')}
              title={t('settings.language.en')}
            >
              EN
            </ToggleButton>
          </ToggleButtonGroup>
        </Box>
        <Box p={1}>
          <Typography variant={'h6'}>{t('settings.sfx')}</Typography>
          <IconButton onClick={handleChangeSfxEnabled} title={t('settings.sfxOnOff')}>
            {sfxEnabled ? <VolumeUpIcon /> : <VolumeOffIcon />}
          </IconButton>
        </Box>
        <Box p={1}>
          <Typography variant={'h6'}>{t('settings.orientation.title')}</Typography>
          <ToggleButtonGroup
            value={orientation}
            exclusive={true}
            onChange={handleChangeOrientation}
            aria-label={t('settings.orientation.title')}
            title={t('settings.orientation.title')}
          >
            <ToggleButton
              value={Orientation.Up}
              aria-label={t('settings.orientation.top')}
              title={t('settings.orientation.top')}
            >
              <ArrowUpwardIcon />
            </ToggleButton>
            <ToggleButton
              value={Orientation.Auto}
              aria-label={t('settings.orientation.auto')}
              title={t('settings.orientation.auto')}
            >
              <AutorenewIcon />
            </ToggleButton>
            <ToggleButton
              value={Orientation.Down}
              aria-label={t('settings.orientation.bottom')}
              title={t('settings.orientation.bottom')}
            >
              <ArrowDownwardIcon />
            </ToggleButton>
          </ToggleButtonGroup>
        </Box>
        <Box p={1}>
          <Typography variant={'h6'}>{t('settings.variant.title')}</Typography>
          <FormControl fullWidth={true}>
            <InputLabel id="demo-simple-select-label">{t('settings.variant.label')}</InputLabel>
            <Select
              value={gameType}
              label={t('settings.variant.label')}
              onChange={handleChangeGameType}
            >
              <MenuItem value="default">{t('settings.variant.default')}</MenuItem>
              <MenuItem value="default_en">{t('settings.variant.default_en')}</MenuItem>
              <MenuItem value="undercover">{t('settings.variant.undercover')}</MenuItem>
              <MenuItem value="duett">{t('settings.variant.duett')}</MenuItem>
            </Select>
          </FormControl>
        </Box>
        <Box p={1}>
          <Typography variant={'h6'}>
            {t('settings.gridSize.title')} ({gridSize}x{gridSize})
          </Typography>
          <Slider
            onChange={handleChangeGridSize}
            aria-label={t('settings.gridSize.title')}
            title={t('settings.gridSize.title')}
            value={gridSize}
            valueLabelDisplay="auto"
            step={1}
            marks={true}
            min={4}
            max={8}
          />
        </Box>
        <Box p={1}>
          <Typography variant={'h6'}>
            {t('settings.bombs.title')} ({bombs})
          </Typography>
          <Slider
            onChange={handleChangeBombs}
            aria-label={t('settings.bombs.title')}
            title={t('settings.bombs.title')}
            value={bombs}
            valueLabelDisplay="auto"
            step={1}
            marks={true}
            min={0}
            max={4}
          />
        </Box>
        <Box p={1}>
          <Typography variant={'h6'}>
            {t('settings.agents.title')} ({agents})
          </Typography>
          <Slider
            onChange={handleChangeAgents}
            aria-label={t('settings.agents.title')}
            title={t('settings.agents.title')}
            value={agents}
            valueLabelDisplay="auto"
            step={1}
            marks={true}
            min={1}
            max={40}
          />
        </Box>
        <Box p={1}>
          <Typography variant={'h6'}>{t('settings.newgame')}</Typography>
          <NewGameButton onClick={handleClose} />
        </Box>
      </Box>
      <Box p={4}>
        <Button variant="contained" color="secondary" size="large" onClick={handleClose}>
          {t('common.close')}
        </Button>
      </Box>
    </Dialog>
  )
}

export default SettingsDialog

import React from 'react'
import BigButton from '../atoms/BigButton'
import { useTranslation } from 'react-i18next'
import { useAppDispatch } from '../app/hooks'
import { startNewGame } from '../Game.reducer'

export interface NewGameButtonProps {
  onClick?: () => void
}

const NewGameButton: React.FC<NewGameButtonProps> = ({ onClick }) => {
  const dispatch = useAppDispatch()
  const handleNewGamePress = () => dispatch(startNewGame())
  const { t } = useTranslation()

  const handleClick = () => {
    handleNewGamePress()
    onClick && onClick()
  }

  return (
    <BigButton variant="contained" color="primary" size="large" onClick={handleClick}>
      {t('common.newGameButton')}
    </BigButton>
  )
}

export default NewGameButton

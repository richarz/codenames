import React, { useState } from 'react'
import { Box, IconButton } from '@mui/material'
import InfoIcon from '@mui/icons-material/Info'
import AboutDialog from './AboutDialog'
import SettingsIcon from '@mui/icons-material/Settings'
import SettingsDialog from './SettingsDialog'
import usePop from '../sounds/usePop'
import usePopDown from '../sounds/usePopDown'
import { useTranslation } from 'react-i18next'

const GameMenu = () => {
  const [openSettingsSound] = usePop()
  const [closeSettingsSound] = usePopDown()
  const [openSettings, setOpenSettings] = useState(false)
  const [openAbout, setOpenAbout] = useState(false)
  const { t } = useTranslation()

  const handleOpenSettings = () => {
    openSettingsSound()
    setOpenSettings(true)
  }
  const handleCloseSettings = () => {
    setOpenSettings(false)
    closeSettingsSound()
  }

  const handleOpenAbout = () => {
    openSettingsSound()
    setOpenAbout(true)
  }
  const handleCloseAbout = () => {
    setOpenAbout(false)
    closeSettingsSound()
  }
  return (
    <Box>
      <IconButton size="large" onClick={handleOpenAbout} title={t('about.title')}>
        <InfoIcon />
      </IconButton>
      <AboutDialog onClose={handleCloseAbout} open={openAbout} />
      <IconButton size="large" onClick={handleOpenSettings} title={t('settings.title')}>
        <SettingsIcon />
      </IconButton>
      <SettingsDialog onClose={handleCloseSettings} open={openSettings} />
    </Box>
  )
}

export default GameMenu

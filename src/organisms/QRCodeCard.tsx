import React from 'react'
import QRCode from 'react-qr-code'
import { useAppSelector } from '../app/hooks'
import { selectCodeCard } from '../Game.reducer'
import { Box } from '@mui/material'
import { useBreakpoint } from '../utils/breakpoints'

const QRCodeCard = () => {
  const codeCard = useAppSelector(selectCodeCard)
  const { isTabletOrBigger, isDesktopOrBigger } = useBreakpoint()

  let size = 40
  if (isDesktopOrBigger) {
    size = 140
  } else if (isTabletOrBigger) {
    size = 95
  }
  return (
    <Box p={isTabletOrBigger ? 1 : 0.25} pb={0} style={{ background: 'rgba(255,255,255,0.2)' }}>
      <QRCode
        size={size}
        value={`${window.location.protocol}//${window.location.host}/?c=${codeCard.join('')}`}
        bgColor="rgba(255,255,255,0)"
        fgColor="rgb(0,0,0)"
        level="L"
      />
    </Box>
  )
}

export default QRCodeCard

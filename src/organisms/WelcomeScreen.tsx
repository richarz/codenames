import React from 'react'
import { Box, Grid, Paper, Typography } from '@mui/material'
import ViewHeightGrid from '../atoms/ViewHeightGrid'
import GameMenu from './GameMenu'
import Version from '../molecules/Version'
import { useBreakpoint } from '../utils/breakpoints'
import { Trans, useTranslation } from 'react-i18next'
import HowToPlayButton from '../molecules/HowToPlayButton'
import NewGameButton from './NewGameButton'

const WelcomeScreen = () => {
  const { isTabletOrBigger } = useBreakpoint()
  const { t } = useTranslation()

  return (
    <ViewHeightGrid container={true} justifyContent="center" alignItems="center">
      <Grid
        item={true}
        container={true}
        xs={12}
        md={8}
        lg={6}
        justifyContent={'center'}
        alignItems={'center'}
        direction={'column'}
        spacing={3}
      >
        <Grid item={true}>
          <Typography variant={isTabletOrBigger ? 'h1' : 'h2'}>{t('appTitle')}</Typography>
        </Grid>
        <Grid item={true}>
          <Paper elevation={6}>
            <Box p={2}>
              <Typography>
                <Trans i18nKey="welcome.welcomeText">
                  Dies ist eine Onlineumsetzung von <em>Codenames</em> dem Kartenspiel.
                  <br />
                  Aber anders als andere Onlineumsetzungen, ist dieses Spiel für den lokalen
                  Multiplayer gedacht.
                  <br />
                  <br />
                  Empfohlen sind <strong>4, 6 oder 8 Spieler</strong> an{' '}
                  <strong>einem PC/Laptop/Tablet</strong>.
                  <br />
                  Auch wird ein <strong>Touch-Display</strong> empfohlen, da dieses Spiel für
                  Touch-Eingaben optimiert wurde.
                  <br />
                  <br />
                  Viel Spaß beim Spielen!
                </Trans>
              </Typography>
              <Grid container={true} justifyContent="center" sx={{ mt: 2 }}>
                <HowToPlayButton />
              </Grid>
            </Box>
          </Paper>
        </Grid>
        <Grid item={true}>
          <NewGameButton />
        </Grid>
        <Grid item={true}>
          <GameMenu />
        </Grid>
        <Grid item={true}>
          <Version />
        </Grid>
      </Grid>
    </ViewHeightGrid>
  )
}

export default WelcomeScreen

import React from 'react'
import AgentCard from './AgentCard'
import { Box, Grid } from '@mui/material'
import { makeStyles } from '@mui/styles'
import { useAppDispatch, useAppSelector } from '../app/hooks'
import {
  exposeAgent,
  selectActiveTeam,
  selectAgents,
  selectComboCounter,
  selectPlayground,
} from '../Game.reducer'
import { FieldType } from '../utils/game'
import { Team } from '../Game.types'
import useBite from '../sounds/useBite'
import useBomb from '../sounds/useBomb'
import useDisableSound from '../sounds/useDisableSound'
import useGlug from '../sounds/useGlug'
import { useBreakpoint } from '../utils/breakpoints'

const useStyle = makeStyles((theme) => ({
  card: {
    textAlign: 'center',
  },
}))

const Playground: React.FC = () => {
  const agents = useAppSelector(selectAgents)
  const { gridSize } = useAppSelector(selectPlayground)
  const activeTeam = useAppSelector(selectActiveTeam)
  const comboCounter = useAppSelector(selectComboCounter)
  const dispatch = useAppDispatch()
  const [cardClickSound] = useBite()
  const [bombSound] = useBomb()
  const [failureSound] = useDisableSound()
  const [glugSound] = useGlug({
    interrupt: true,
  })
  const { isTabletOrBigger } = useBreakpoint()

  const classes = useStyle()
  const handleCardClick = (agent: string, type: FieldType) => {
    cardClickSound()
    if (type === FieldType.Bomb) {
      bombSound()
    }
    if (type === FieldType.Red && activeTeam !== Team.Red) {
      failureSound()
    } else if (type === FieldType.Blue && activeTeam !== Team.Blue) {
      failureSound()
    } else if (type === FieldType.PasserBy) {
      failureSound()
    } else {
      // it was the right card and the comboCounter increases
      // no sound at the moment
      if (comboCounter >= 2) {
        glugSound({
          playbackRate: 1 + 0.2 * comboCounter,
        })
      }
    }
    dispatch(exposeAgent(agent))
  }
  return (
    <Box p={isTabletOrBigger ? 2 : 0.5}>
      <Grid
        container={true}
        spacing={isTabletOrBigger ? 2 : 0.5}
        columns={gridSize}
        justifyContent="center"
      >
        {agents.map((agent: string, index: number) => (
          <Grid item={true} xs={1} className={classes.card} key={`agent-${agent}`}>
            <AgentCard agent={agent} index={index} onClick={handleCardClick} />
          </Grid>
        ))}
      </Grid>
    </Box>
  )
}
export default Playground

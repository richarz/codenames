import React from 'react'
import { Box, Grid, Paper } from '@mui/material'
import CodeCard from './organisms/CodeCard'
import Playground from './organisms/Playground'
import {
  selectActiveTeam,
  selectExposedBlueAgents,
  selectExposedRedAgents,
  selectPlayground,
  selectSecretServiceChief,
  selectWinner,
} from './Game.reducer'
import { useAppSelector } from './app/hooks'
import { makeStyles } from '@mui/styles'
import classNames from 'classnames'
import { Orientation, Side, Team } from './Game.types'
import WinningScreen from './organisms/WinningScreen'
import ViewHeightGrid from './atoms/ViewHeightGrid'
import WelcomeScreen from './organisms/WelcomeScreen'
import GameMenu from './organisms/GameMenu'
import Version from './molecules/Version'
import TeamControl from './organisms/TeamControl'

const useStyle = makeStyles(() => ({
  playground: {
    transition: 'transform 250ms',
  },
  mirrored: {
    transform: 'scale(-1, -1)',
  },
}))

function App() {
  const queryParams = new URLSearchParams(window.location.search)
  const codeCard = queryParams.get('c')
  const classes = useStyle()
  const activeTeam = useAppSelector(selectActiveTeam)
  const exposedRedAgents = useAppSelector(selectExposedRedAgents)
  const exposedBlueAgents = useAppSelector(selectExposedBlueAgents)
  const secretServiceChiefSide = useAppSelector(selectSecretServiceChief)
  const { orientation } = useAppSelector(selectPlayground)
  const winner = useAppSelector(selectWinner)

  const isLeftSide =
    orientation === Orientation.Auto ? secretServiceChiefSide === Side.Left : orientation
  const isRedTeam = activeTeam === Team.Red

  const gameScreen = (
    <ViewHeightGrid container={true} columns={50}>
      <TeamControl
        side={Side.Left}
        team={Team.Red}
        active={isRedTeam}
        mirrored={!isLeftSide}
        exposedAgents={exposedRedAgents}
      />
      <Grid
        item={true}
        container={true}
        className={classNames(classes.playground, {
          [classes.mirrored]: !isLeftSide,
        })}
        id="playground"
        xs={32}
        md={36}
        justifyContent={'start'}
        alignItems={'center'}
        direction="column"
      >
        <Playground />
        <GameMenu />
        <Version />
      </Grid>
      <TeamControl
        side={Side.Right}
        team={Team.Blue}
        active={!isRedTeam}
        mirrored={!isLeftSide}
        exposedAgents={exposedBlueAgents}
      />
    </ViewHeightGrid>
  )

  if (codeCard) {
    return (
      <Grid container={true} justifyContent="center" alignItems="center">
        <Paper>
          <Box pt={2} pl={2}>
            <CodeCard codeCard={codeCard.split('').map(Number)} />
          </Box>
        </Paper>
      </Grid>
    )
  }

  if (activeTeam !== undefined) {
    return (
      <>
        {gameScreen}
        <WinningScreen show={winner !== undefined} />
      </>
    )
  }
  return <WelcomeScreen />
}

export default App

import { CodeCardOptions } from './utils/game'

export enum Team {
  Blue,
  Red,
}

export enum Side {
  Left,
  Right,
}

export interface SessionState {
  redAgentsCount: number
  blueAgentsCount: number
  activeTeam: Team | undefined
  secretServiceChief: Side
  comboCounter: number
  exposedAgents: string[]
  exposedRedAgents: string[]
  exposedBlueAgents: string[]
  winner: Team | undefined
  winCounter: {
    red: number
    blue: number
  }
}

export interface GameState {
  codecard: number[]
  agents: string[]
  config: CodeCardOptions
  state: SessionState
}

export enum Orientation {
  Up = Side.Left,
  Auto = 3,
  Down = Side.Right,
}

import { red } from '@mui/material/colors'
import { createTheme } from '@mui/material/styles'
import darkScrollbar from '@mui/material/darkScrollbar'
import WoodPatternBackground from './assets/wood-pattern.png'

const palette = {
  mode: 'dark',
  primary: {
    main: '#934ee8',
  },
  secondary: {
    main: '#1c9dc2',
  },
  error: {
    main: red.A400,
  },
}

// A custom theme for this app
const theme = createTheme({
  typography: {
    fontSize: 12,
  },
  // @ts-ignore
  palette,
  shadows: [
    'none',
    '0px 0px 1px -1px rgba(0,0,0,0.4),0px 0px 1px 0px rgba(0,0,0,0.24),0px 0px 3px 0px rgba(0,0,0,0.22)',
    '0px 0px 1px -2px rgba(0,0,0,0.4),0px 0px 2px 0px rgba(0,0,0,0.24),0px 0px 5px 0px rgba(0,0,0,0.22)',
    '0px 0px 3px -2px rgba(0,0,0,0.4),0px 0px 4px 0px rgba(0,0,0,0.24),0px 0px 8px 0px rgba(0,0,0,0.22)',
    '0px 0px 4px -1px rgba(0,0,0,0.4),0px 0px 5px 0px rgba(0,0,0,0.24),0px 0px 10px 0px rgba(0,0,0,0.22)',
    '0px 0px 5px -1px rgba(0,0,0,0.4),0px 0px 8px 0px rgba(0,0,0,0.24),0px 0px 14px 0px rgba(0,0,0,0.22)',
    '0px 0px 5px -1px rgba(0,0,0,0.4),0px 0px 10px 0px rgba(0,0,0,0.24),0px 0px 18px 0px rgba(0,0,0,0.22)',
    '0px 0px 5px -2px rgba(0,0,0,0.4),0px 0px 10px 1px rgba(0,0,0,0.24),0px 0px 16px 1px rgba(0,0,0,0.22)',
    '0px 0px 5px -3px rgba(0,0,0,0.4),0px 0px 10px 1px rgba(0,0,0,0.24),0px 0px 14px 2px rgba(0,0,0,0.22)',
    '0px 0px 6px -3px rgba(0,0,0,0.4),0px 0px 12px 1px rgba(0,0,0,0.24),0px 0px 16px 2px rgba(0,0,0,0.22)',
    '0px 0px 6px -3px rgba(0,0,0,0.4),0px 0px 14px 1px rgba(0,0,0,0.24),0px 0px 18px 3px rgba(0,0,0,0.22)',
    '0px 0px 7px -4px rgba(0,0,0,0.4),0px 0px 15px 1px rgba(0,0,0,0.24),0px 0px 20px 3px rgba(0,0,0,0.22)',
    '0px 0px 8px -4px rgba(0,0,0,0.4),0px 0px 17px 2px rgba(0,0,0,0.24),0px 0px 22px 4px rgba(0,0,0,0.22)',
    '0px 0px 8px -4px rgba(0,0,0,0.4),0px 0px 19px 2px rgba(0,0,0,0.24),0px 0px 24px 4px rgba(0,0,0,0.22)',
    '0px 0px 9px -4px rgba(0,0,0,0.4),0px 0px 21px 2px rgba(0,0,0,0.24),0px 0px 26px 4px rgba(0,0,0,0.22)',
    '0px 0px 9px -5px rgba(0,0,0,0.4),0px 0px 22px 2px rgba(0,0,0,0.24),0px 0px 28px 5px rgba(0,0,0,0.22)',
    '0px 0px 10px -5px rgba(0,0,0,0.4),0px 0px 24px 2px rgba(0,0,0,0.24),0px 0px 30px 5px rgba(0,0,0,0.22)',
    '0px 0px 11px -5px rgba(0,0,0,0.4),0px 0px 26px 2px rgba(0,0,0,0.24),0px 0px 32px 5px rgba(0,0,0,0.22)',
    '0px 0px 11px -5px rgba(0,0,0,0.4),0px 0px 28px 2px rgba(0,0,0,0.24),0px 0px 34px 6px rgba(0,0,0,0.22)',
    '0px 0px 12px -6px rgba(0,0,0,0.4),0px 0px 29px 2px rgba(0,0,0,0.24),0px 0px 36px 6px rgba(0,0,0,0.22)',
    '0px 0px 13px -6px rgba(0,0,0,0.4),0px 0px 31px 3px rgba(0,0,0,0.24),0px 0px 38px 7px rgba(0,0,0,0.22)',
    '0px 0px 13px -6px rgba(0,0,0,0.4),0px 0px 33px 3px rgba(0,0,0,0.24),0px 0px 40px 7px rgba(0,0,0,0.22)',
    '0px 0px 14px -6px rgba(0,0,0,0.4),0px 0px 35px 3px rgba(0,0,0,0.24),0px 0px 42px 7px rgba(0,0,0,0.22)',
    '0px 0px 14px -7px rgba(0,0,0,0.4),0px 0px 36px 3px rgba(0,0,0,0.24),0px 0px 44px 8px rgba(0,0,0,0.22)',
    '0px 0px 15px -7px rgba(0,0,0,0.4),0px 0px 38px 3px rgba(0,0,0,0.24),0px 0px 46px 8px rgba(0,0,0,0.22)',
  ],
  components: {
    MuiCssBaseline: {
      styleOverrides: {
        body: {
          ...darkScrollbar(),
          background: `url(${WoodPatternBackground}), radial-gradient(circle, rgba(50,50,50,1) 0%, rgba(18,18,18,1) 56%)`,
          overflow: 'hidden',
        },
      },
    },
  },
})

export default theme
